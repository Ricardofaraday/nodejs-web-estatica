var http = require("http");
var path = require("path");	//Este modulo permite manipular rutas
var fs   = require("fs"); //Este modulo permite manipular ficheros

function iniciar( ){

	function conexion (request, response){		
		var archivo = "";
		if (request.url == "/")
			archivo = "/index.html";
		else
			archivo = `${request.url}`		

		var extension   = path.extname(archivo)	//obtenemos la extension
		var carpeta     = "/views";
		var contentType = "text/html";

		switch(extension){
			case ".html":
				break;
			case ".css":
				contentType = "text/css";
				carpeta     += "/css";
				break;
			case ".js":
				contentType = "text/javascript";
				carpeta     += "/js";
				break;
		}

		var direccion = path.join(".", carpeta, archivo);
		responder(response, direccion, contentType);

		console.log(`01-Se recibio una peticion para:
		=> ${request.url} 
		=> extension: ${extension}
		=> direccion: ${direccion}
		`);
	
	}

	function responder(response, direccion, contentType){

		console.log(direccion);
		fs.exists(direccion, function (existe){
			if (existe){
				fs.readFile(direccion, function(error, content){
					if (error){
						response.writeHead(500);
						response.end();
					} else {
						response.writeHead(200, {"content-type":contentType});
						response.end(content);
					}	
				});

			} else {
				response.write(`<h2 style="color: #aa0000;">404 No se encontro la pagina</h2>`);
				response.end();
			}
		});

	}

	http.createServer(conexion).listen(8787);
	console.log("00-El servidor acaba de ser iniciado en el puerto 8787");
}


// module.exports.iniciar = iniciar;
iniciar()
